<?php

class Ok_like {
	protected $db = null;
	protected $token_order = 0;
	protected $token = null;
	protected $user_token = null;

	function __construct($db){
		$this->db = $db;
		$this->random_token();
	}

	private function random_token() {
		$query = "SELECT token FROM tokens ORDER BY RANDOM() LIMIT 1";
		$res = $this->db->query($query);
		$token = $res->fetchArray();
		$this->token = $token['token'];
	}

	private function find_token($token) {
		$query = "SELECT COUNT(token) AS ct FROM tokens WHERE token = '{$token}'";
		$res = $this->db->query($query);
		$val = $res->fetchArray();

		return $val['ct'];
	}

	public function add_token($token) {
		$num = $this->find_token($token);

		if($num != 0){
			return false;
		}

		$query = "INSERT INTO tokens(token) VALUES('$token')";
		return $this->db->query($query);
	}

	private function delete_token(){
		if(empty($this->token)){
			return false;
		}

		$query = "DELETE FROM tokens WHERE token='{$this->token}'";
		return $this->db->query($query);
	}

	public function login() {
		$url = 'http://ok-like.net/ajax/login.php?token=' . $this->token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$info = curl_getinfo($ch);
		$result = curl_exec($ch);
		curl_close($ch);

		if(preg_match('/Welcome/', $result)){
			return true;
		}else{
			$this->delete_token();
			$this->random_token();
			$this->login();
		}
	}

	public function get_action() {
		$url = 'http://ok-like.net/ajax/do.php?id=0_0&token=' . $this->token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$info = curl_getinfo($ch);
		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	} 
}

?>