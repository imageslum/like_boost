<?php

	class Like{
		private $id = null;
		private $db = null;
		private $token = null;

		function __construct($db) {
			$this->db = $db;
		}

		private function random_token() {
			$query = "SELECT token FROM tokens ORDER BY RAND() LIMIT 1";
			$res = mysql_query($query);
			$token = mysql_fetch_assoc($res);
			$this->token = $token['token'];
		}

		public function set_post_id($id) {
			$this->id = $id;

			return $this;
		}

		public function take_like($type = 'post') {
			$this->random_token();

			if($this->find_liked() != 0) {
				return false;
			}

			if($type == 'post') {
				$url = "https://graph.facebook.com/{$this->id}/likes?method=post&access_token={$this->token}";
			}

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$info = curl_getinfo($ch);
			$result = curl_exec($ch);
			curl_close($ch);

			if($result == 'true'){
				$this->add_liked();
				return true;
			}else{

				preg_match('/1705/', $result, $match);
				if(sizeof($match) > 0){
					$this->add_liked();
				}else{
					$this->delete_token();
				}
				$this->delete_token();
				return false;
			}
		}

		private function add_liked() {
			$query = "INSERT INTO used(token, post_id) VALUES('{$this->token}', '{$this->id}')";
			return mysql_query($query);
		}

		private function delete_token(){
			if(empty($this->token)){
				return false;
			}

			$query = "DELETE FROM tokens WHERE token='{$this->token}'";
			return mysql_query($query);
		}

		private function find_liked() {
			$query = "SELECT COUNT(id) AS ct FROM used WHERE token = '{$this->token}' AND post_id = '{$this->id}'";
			$res = mysql_query($query);
			$val = mysql_fetch_assoc($res);
			return $val['ct'];
		}
	}

?>
