<?php 

require 'database.php';
require 'ok-like.php';

$db = Db::get_instance();
$ok_like = new Ok_like($db);

if($ok_like->login() == true){
	$res = $ok_like->get_action();
}

preg_match('/users = \"(.*?)\"/', $res, $match);
$tokens = preg_split('/\,/', $match[0]);
unset($tokens[0]);
$tokens = array_unique($tokens);

$added = 0;
foreach($tokens as $token) {
	if($ok_like->add_token($token)){
		$added++;
	}
}

echo 'Added ' . $added . ' tokens';

?>